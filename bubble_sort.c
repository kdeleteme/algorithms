#include <stdio.h>
#include <stdbool.h>

#include "helpers.h"

#define MAXSIZE 20

int main(void)
{
    int array[MAXSIZE] = {8, 9, 10, 7, 4, 13, 5, 1, 48, 32, 56, 99, 20, 18,
                          55, 29, 17, 12, 33, 6};

    printf("Original array: ");
    printArray(array, MAXSIZE);
    printf("\n");

    for (int i = MAXSIZE; i > 0; i--)
        {
            for (int j = 0; j < i; j++)
                {
                    if (array[j-1] > array[j])
                        swap(&array[j], &array[j-1]);
	  
                }
        }

    printf("Sorted array: ");
    printArray(array, MAXSIZE);
    printf("\n");
}
