#ifndef HELPERS_H
#define HELPERS_H

void printArray(int *arr, int arr_size);
void swap(int *left, int *right);

#endif
