#include <stdio.h>

#include "helpers.h"

#define MAXSIZE 20
int main(void)
{
    int arr[MAXSIZE] = {4, 23, 8, 9, 7, 10, 2, 16, 8, 9, 10, 7, 4, 13, 5, 1,
                        48, 32, 56, 99};

    printf("Original Array: ");
    printArray(arr, MAXSIZE);
    printf("\n");

    // i will be the current unsorted index
    for (int i = 0; i < MAXSIZE; i++)
        {
            int lowest = i;
            for (int j = lowest; j < MAXSIZE; j++)
                {
                    if (arr[j] < arr[lowest])
                        lowest = j;
                }
            swap(&arr[lowest], &arr[i]);	          
        }

    printf("Sorted Array: ");
    printArray(arr, MAXSIZE);
    printf("\n");
}
