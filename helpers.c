#include <stdio.h>

#include "helpers.h"

void printArray(int *arr, int arr_size)
{
  for (int i = 0; i < arr_size; i++)
    printf("%d ", arr[i]);
}

void swap(int *left, int *right)
{
  int temp = *left;
  *left = *right;
  *right = temp;
}
